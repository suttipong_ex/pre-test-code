import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> number = new ArrayList<>();
        number.add(1);
        number.add(9);
        number.add(2);
        number.add(3);
        System.out.println("Find max => " + findMax(number));
        System.out.println("Find min => " + findMin(number));
        testExample1();
        testExample2();
        testExample3();
    }
    private static Integer findMax(ArrayList<Integer> num){
        Integer max = 0;
        for(Integer i = 0 ; i < num.size() ; i++){
            if(num.get(i) > max){
                max =  num.get(i);
            }
        }
        return max;
    }
    private static Integer findMin(ArrayList<Integer> num){
        Integer min = Integer.MAX_VALUE;
        for(Integer i = 0 ; i < num.size() ; i++){
            if(num.get(i) < min){
                min =  num.get(i);
            }
        }
        return min;
    }
    public static void testExample1() {

        int inputNo = 6;
        for(int i = 1 ; i <= inputNo ; i++) {
            int sum = process1(i);
            System.out.print("input n=" + i);
            System.out.print(" Process = "+i+" Output => "+sum);
            System.out.println();
        }
    }
    public static int process1(int n){
        if(n != 0){
            return n + process1(n - 1);
        }else{
            return n;
        }
    }
    public static void testExample2() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println("output" + process2(list,2));
    }
    public static List<Integer> process2(List<Integer> numberList, int chunk) {
        List<Integer> result  = new ArrayList<>();
        int sumChk;
        int i = 0;
        for(Integer e :numberList){
            sumChk = 0;
            for(int j = 0 ; j < chunk ; j++){
                if(i == numberList.size()){
                    break;
                }
                sumChk += numberList.get(i);
                i++;
            }
            result.add(sumChk);
            if(i == numberList.size()){
                break;
            }
        }
        return result;
    }
    public static void testExample3() {
        System.out.println("output = " + process3(4,6,2,4));
    }
    public static int process3(int sizeX, int sizeY, int elementX, int elementY) {
        int result = 0;
        int[][] array = new int[sizeX][sizeY];
        array[elementX][elementY] = 1;
        for(int i = elementX; i < sizeX; i++){
            for(int j = elementY; j < sizeY; j++){
                array[elementX][elementY] = 1;
                elementY++;
            }
            elementX++;
            result++;
        }
        System.out.println();

        for(int i = 0; i < sizeX; i++){
            for(int j = 0; j < sizeY; j++){
                System.out.print(" array["+i+"]["+j+"] value : "+ array[i][j]);
            }
            System.out.println();
        }
        System.out.println();

        return result;
    }
}